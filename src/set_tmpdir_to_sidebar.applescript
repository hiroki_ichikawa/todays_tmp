

tell application "Finder"
	activate


	set tmpFolder to ((path to home folder) as text) & "tmp"
	set todaysTmpFolder to tmpFolder & ":todays_tmp"
	open folder tmpFolder
	select todaysTmpFolder

	set theID to id of window 1

	delay 0.5

	tell application "System Events"
		keystroke "t" using {control down, command down}
	end tell

	delay 0.5

	tell the window id theID to close
end tell
