#!/bin/bash

current_dir=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
src_dir=$current_dir"/src"


# create app directory
if [ ! -e ~/bin ]; then
        mkdir ~/bin
fi
bin_dir=~/bin/make_tmpdir
mkdir "$bin_dir"

# copy files
cp "$src_dir/app.sh" "$bin_dir"
chmod 775 "$src_dir/app.sh" "$bin_dir"
cp "$src_dir/set_tmpdir_to_sidebar.applescript" "$bin_dir"

if [ ! -e ~/Library/LaunchAgents ]; then
        mkdir ~/Library/LaunchAgents
fi
plist_path=~/Library/LaunchAgents/make_tmpdir.plist
/usr/bin/sed -e "s@__HOME__@$HOME@g" "$src_dir/make_tmpdir.template.plist" > "$plist_path"

# load launcd
launchctl unload "$plist_path"
launchctl load "$plist_path"

# set alias
bashrc_path=~/.bashrc
if [ ! -e ~/.bashrc ]; then
        cp "$src_dir/bashrc.template" "$bashrc_path"
else
        echo "alias tmp='source ~/bin/make_tmpdir/app.sh'" >> "$bashrc_path"
fi

bash_profile_path=~/.bash_profile
if [ ! -e ~/.bash_profile ]; then
        cp "$src_dir/bash_profile.template" "$bash_profile_path"
fi


source "$bashrc_path"

source ~/bin/make_tmpdir/app.sh
